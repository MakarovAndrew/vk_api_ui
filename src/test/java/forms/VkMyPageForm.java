package forms;

import models.Comment;
import models.Post;
import org.openqa.selenium.By;

public class VkMyPageForm extends BaseForm {

    public VkMyPageForm(Post post) {
        super(By.xpath(
                String.format(
                        "//div[@id='submit_post_box' and @data-oid='%s']", post.getOwnerId())
        ), "My page form");
    }

    public void clickLikePostLabel(Post post) {
        getElementFactory().getLabel(
                By.xpath(
                        String.format("//div[@data-reaction-target-object='wall%s_%s']", post.getOwnerId(), post.getPostId())
                ), "Like the new post"
        ).click();
    }

    public boolean isCommentDisplayed(Comment comment) {
        return getElementFactory().getLabel(
                By.xpath(
                        String.format("//div[@id='wpt%s_%s']", comment.getOwnerId(), comment.getCommentId())
                ), "New comment label"
        ).state().waitForDisplayed();
    }

    public boolean isPostNotDisplayed(Post post) {
        return getElementFactory().getLabel(
                By.id(
                        String.format("wpt%s_%s", post.getOwnerId(), post.getPostId())
                ),      String.format("Post #%s label", post.getPostId())
        ).state().waitForNotDisplayed();
    }

    public void clickShowNextCommentLink(Post post) {
        getElementFactory().getLink(
                By.xpath(
                        String.format("//a[@href='/wall%s_%s']/span[@class='js-replies_next_label']", post.getOwnerId(), post.getPostId())
                ), "Show next comment link"
        ).click();
    }

    public boolean isImageDisplayedForPost(Post post) {
        return getElementFactory().getLabel(
                        By.xpath(
                                String.format("//a[@href='/%s']", post.getPhotoString())
                        ), "Added picture label"
                ).state().isExist();
    }

    public String getPostText(Post post) {
        return getElementFactory().getLabel(
                By.id(
                        String.format("wpt%s_%s", post.getOwnerId(), post.getPostId())
                ),      String.format("Post #%s label", post.getPostId())
        ).getText();
    }
}