package forms;

import aquality.selenium.elements.ElementType;
import aquality.selenium.elements.interfaces.ILabel;
import org.apache.commons.lang3.StringUtils;
import org.openqa.selenium.By;

public class VkLeftPanelForm extends BaseForm {
    private final ILabel myPageLabel = getElementFactory().getLabel(By.id("l_pr"), "The 'My page' label");

    public VkLeftPanelForm() {
        super(By.className("vkui__root"), "Left panel form");
    }

    public void clickMyPageLabel() {
        myPageLabel.click();
    }

    public String getOwnerId() {
        return StringUtils.substringAfter(
                myPageLabel.findChildElement(
                        By.xpath("./a[contains(@class,'LeftMenuItem-module__item')]"), ElementType.LINK
                ).getAttribute("href"), "id"
        );
    }
}