package models;

import lombok.Getter;
import lombok.NoArgsConstructor;

@NoArgsConstructor @Getter
public class VkApiConfig {
    private String apiScheme;
    private String apiHost;
    private String apiPathSegments;
    private String apiVersion;
}