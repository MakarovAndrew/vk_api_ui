package vkApiUtils;

import okhttp3.HttpUrl;
import java.util.Map;
import static utils.ConfigManager.credentials;
import static utils.ConfigManager.vkApiConfig;

public class BaseApiUtils {

    protected static HttpUrl getFullUrl(String endpoint, Map<String, String> parameters) {
        HttpUrl.Builder url = new HttpUrl.Builder()
                .scheme(vkApiConfig.getApiScheme())
                .host(vkApiConfig.getApiHost())
                .addPathSegments(vkApiConfig.getApiPathSegments())
                .addPathSegment(endpoint)
                .addQueryParameter("access_token", credentials.getToken())
                .addQueryParameter("v", vkApiConfig.getApiVersion());
        if (parameters != null) {
            for (Map.Entry<String, String> entry : parameters.entrySet()) {
                url.addQueryParameter(entry.getKey(), entry.getValue());
            }
        }
        return url.build();
    }
}