package models;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor @Getter @Setter
public class Post {
    private String postId;
    private String ownerId;
    private String photoId;
    private String text;

    public Post(String ownerId) {
        this.ownerId = ownerId;
    }

    public String getPhotoString() {
        return String.format("photo%s_%s",
                this.ownerId,
                this.photoId
        );
    }
}