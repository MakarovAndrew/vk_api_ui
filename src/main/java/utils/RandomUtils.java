package utils;

import static org.apache.commons.lang3.RandomStringUtils.randomAlphabetic;

public class RandomUtils {

    public static String randomAlphabeticString(int length) {
        return randomAlphabetic(length);
    }
}