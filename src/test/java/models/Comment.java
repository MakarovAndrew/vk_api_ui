package models;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor @Getter @Setter
public class Comment {
    private String commentId;
    private String text;
    private String postId;
    private String ownerId;

    public Comment(String postId, String ownerId) {
        this.postId = postId;
        this.ownerId = ownerId;
    }
}