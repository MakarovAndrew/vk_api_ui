package utils;

import models.Credentials;
import models.TestConfig;
import models.TestData;
import models.VkApiConfig;
import java.io.File;
import java.nio.file.Paths;

public class ConfigManager {
    private final static String PATH_TO_RESOURCES = "./src/test/resources/";
    public final static File IMAGE_FILE = Paths.get(PATH_TO_RESOURCES + "anyPicture.jpg").toFile();
    public final static TestData testData = JsonUtils.readObjectFromJsonFile(Paths.get(PATH_TO_RESOURCES + "testData.json"), TestData.class);
    public final static Credentials credentials = JsonUtils.readObjectFromJsonFile(Paths.get(PATH_TO_RESOURCES + "credentials.json"), Credentials.class);
    public final static TestConfig testConfig = JsonUtils.readObjectFromJsonFile(Paths.get(PATH_TO_RESOURCES + "testConfig.json"), TestConfig.class);
    public final static VkApiConfig vkApiConfig = JsonUtils.readObjectFromJsonFile(Paths.get(PATH_TO_RESOURCES + "vkApiConfig.json"), VkApiConfig.class);
}