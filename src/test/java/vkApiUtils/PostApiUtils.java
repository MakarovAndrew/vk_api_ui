package vkApiUtils;

import models.Comment;
import models.Post;
import okhttp3.HttpUrl;
import utils.HttpUtils;
import utils.JsonUtils;
import java.util.Map;

public class PostApiUtils extends BaseApiUtils{

    public static String addPost(Post post) {
        final Map<String,String> queryParameters = Map.of(
                "message", post.getText()
        );
        final HttpUrl url = getFullUrl("wall.post", queryParameters);
        return JsonUtils.getValueByKeyFromJsonString(
                HttpUtils.sendPostRequest(url), "post_id"
        );
    }

    public static void editPost(Post post) {
        final Map<String,String> queryParameters = Map.of(
                "post_id",post.getPostId(),
                "message", post.getText(),
                "attachments", post.getPhotoString()
        );
        final HttpUrl url = getFullUrl("wall.edit", queryParameters);
        HttpUtils.sendPostRequest(url);
    }

    public static String addComment(Comment comment) {
        final Map<String,String> queryParameters = Map.of(
                "message", comment.getText(),
                "post_id", comment.getPostId()
        );
        final HttpUrl url = getFullUrl("wall.createComment", queryParameters);
        return JsonUtils.getValueByKeyFromJsonString(
                HttpUtils.sendPostRequest(url), "comment_id"
        );
    }

    public static boolean isPostLiked(Post post) {
        final Map<String,String> queryParameters = Map.of(
                "type", "post",
                "item_id", post.getPostId()
        );
        final HttpUrl url = getFullUrl("likes.isLiked", queryParameters);
        return Integer.parseInt(
                JsonUtils.getValueByKeyFromJsonString(
                        HttpUtils.sendPostRequest(url), "liked"
                )
        ) == 1;
    }

    public static void deletePost(Post post) {
        final Map<String,String> queryParameters = Map.of(
                "post_id", post.getPostId()
        );
        final HttpUrl url = getFullUrl("wall.delete", queryParameters);
        HttpUtils.sendPostRequest(url);
    }
}