package utils;

import aquality.selenium.core.utilities.ISettingsFile;
import aquality.selenium.core.utilities.JsonSettingsFile;

public class HttpUtilsConfigManager {
    public final static ISettingsFile settings = new JsonSettingsFile("settings.json");
}