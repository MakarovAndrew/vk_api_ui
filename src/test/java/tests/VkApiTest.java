package tests;

import aquality.selenium.core.logging.Logger;
import forms.VkEnterPasswordForm;
import forms.VkLeftPanelForm;
import forms.VkLoginForm;
import forms.VkMyPageForm;
import models.Comment;
import models.Post;
import models.VkPhotosUploadPicture;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;
import vkApiUtils.PostApiUtils;
import vkApiUtils.UploadApiUtils;
import static aquality.selenium.browser.AqualityServices.getBrowser;
import static utils.ConfigManager.*;
import static utils.RandomUtils.randomAlphabeticString;

public class VkApiTest {
    private static final String BASE_URL = testConfig.getBaseUrl();

    @BeforeTest
    protected void beforeTest() {
        getBrowser().maximize();
        getBrowser().goTo(BASE_URL);
    }

    @AfterTest
    public void afterTest() {
        getBrowser().quit();
    }

    @Test
    public void checkSendingVkApiRequestsCombinedWithUiHandling() {
        final SoftAssert softAssert = new SoftAssert();
        final int TEXT_LENGTH = testData.getTextLength();

        Logger.getInstance().info("Performing STEP 1: [UI] Navigate to the main page...");
            final VkLoginForm vkLoginForm = new VkLoginForm();

        Logger.getInstance().info("Performing STEP 2: [UI] Sign in to the site...");
            vkLoginForm.enterLogin(credentials.getLogin());
            vkLoginForm.clickSignInButton();
            final VkEnterPasswordForm vkEnterPasswordForm = new VkEnterPasswordForm();
            vkEnterPasswordForm.enterPassword(credentials.getPassword());
            vkEnterPasswordForm.clickContinueButton();
            final VkLeftPanelForm vkLeftPanelForm = new VkLeftPanelForm();

        Logger.getInstance().info("Performing STEP 3: [UI] Navigate to 'My page'");
            vkLeftPanelForm.clickMyPageLabel();
            final Post newPost = new Post(vkLeftPanelForm.getOwnerId());
            final VkMyPageForm vkMyPageForm = new VkMyPageForm(newPost);

        Logger.getInstance().info("Performing STEP 4: [API] Create random post and get post id from response");
            newPost.setText(randomAlphabeticString(TEXT_LENGTH));
            newPost.setPostId(PostApiUtils.addPost(newPost));

        Logger.getInstance().info("Performing STEP 5: [UI] Created post is on the page with correct value and id");
            Assert.assertEquals(vkMyPageForm.getPostText(newPost), newPost.getText(),
                    String.format(
                            "Post's text with ID='%s' doesn't match initial value!\nExpected: '%s'\nActual  : '%s'",
                                newPost.getPostId(),
                                newPost.getText(),
                                vkMyPageForm.getPostText(newPost)
                    )
            );

        Logger.getInstance().info("Performing STEP 6: [API] Edit created post and add a picture");
            final VkPhotosUploadPicture vkPhotosUploadPicture = UploadApiUtils.uploadPictureToServer(
                    UploadApiUtils.getWallUploadServerUrl(), IMAGE_FILE
            );
            newPost.setPhotoId(
                    UploadApiUtils.savePictureToServer(vkPhotosUploadPicture)
            );
            newPost.setText(randomAlphabeticString(TEXT_LENGTH));
            PostApiUtils.editPost(newPost);

        Logger.getInstance().info("Performing STEP 7: [UI] Text has changed and displayed picture equals to uploaded picture");
            softAssert.assertEquals(vkMyPageForm.getPostText(newPost), newPost.getText(),
                    String.format(
                            "Post's text with ID='%s' doesn't match changed value!\nExpected: '%s'\nActual  : '%s'",
                            newPost.getPostId(),
                            newPost.getText(),
                            vkMyPageForm.getPostText(newPost)
                    )
            );
            softAssert.assertTrue(vkMyPageForm.isImageDisplayedForPost(newPost),
                    String.format(
                            "Photo with id '%s' does not exist on the page!",
                            newPost.getPhotoString()
                    )
            );
            softAssert.assertAll();

        Logger.getInstance().info("Performing STEP 8: [API] Add random comment to crated post");
            final Comment newComment = new Comment(newPost.getPostId(), newPost.getOwnerId());
            newComment.setText(randomAlphabeticString(TEXT_LENGTH));
            newComment.setCommentId(PostApiUtils.addComment(newComment));

        Logger.getInstance().info("Performing STEP 9: [UI] Comment has added to created post by right user");
            vkMyPageForm.clickShowNextCommentLink(newPost);
            Assert.assertTrue(vkMyPageForm.isCommentDisplayed(newComment),
                    String.format(
                            "Comment with id:'%s' does not exist!",
                            newComment.getCommentId()
                    )
            );

        Logger.getInstance().info("Performing STEP 10: [UI] Like added post");
            vkMyPageForm.clickLikePostLabel(newPost);

        Logger.getInstance().info("Performing STEP 11: [API] Added post has liked by right user");
            Assert.assertTrue(PostApiUtils.isPostLiked(newPost), "Added comment has not liked by right user!");

        Logger.getInstance().info("Performing STEP 12: [API] Delete added post");
            PostApiUtils.deletePost(newPost);

        Logger.getInstance().info("Performing STEP 13: [UI] Added post has not displayed");
            Assert.assertTrue(vkMyPageForm.isPostNotDisplayed(newPost), "Added post still displayed!");
    }
}