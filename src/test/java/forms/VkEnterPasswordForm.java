package forms;

import aquality.selenium.elements.interfaces.IButton;
import aquality.selenium.elements.interfaces.ITextBox;
import org.openqa.selenium.By;

public class VkEnterPasswordForm extends BaseForm {
    private final ITextBox inputPasswordTextBox = getElementFactory().getTextBox(By.xpath("//input[@type='password']"), "The 'Password' filed");
    private final IButton continueButton = getElementFactory().getButton(By.className("vkuiButton__in"), "The 'Continue' button");

    public VkEnterPasswordForm() {
        super(By.className("vkc__AuthRoot__contentIn"), "VK password form");
    }

    public void enterPassword(String password) {
        inputPasswordTextBox.clearAndType(password);
    }

    public void clickContinueButton() {
        continueButton.click();
    }
}