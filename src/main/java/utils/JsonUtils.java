package utils;

import aquality.selenium.core.logging.Logger;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.IOException;
import java.nio.file.Path;

public class JsonUtils {
    private static final ObjectMapper mapper = new ObjectMapper().enable(DeserializationFeature.FAIL_ON_TRAILING_TOKENS);

    public static String getValueByKeyFromJsonString(String jsonString, String key) {
        try {
            Logger.getInstance().info("Reading value by key:'%s' from the JSON string...", key);
            JsonNode rootNode = mapper.readTree(jsonString).findPath(key);
            return rootNode.asText();
        } catch (JsonProcessingException e) {
            throw new RuntimeException(e);
        }
    }

    public static <T> T readObjectFromJsonString(String jsonString, Class<T> clazz) {
        try {
            Logger.getInstance().info("Reading '%s' from the JSON string...", clazz.getSimpleName());
            return mapper.readValue(jsonString, clazz);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public static <T> T readObjectFromJsonFile(Path pathToJson, Class<T> clazz) {
        try {
            Logger.getInstance().info("Reading '%s' from file :: '%s' ...", clazz.getSimpleName(), pathToJson);
            return mapper.readValue(pathToJson.toFile(), clazz);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}