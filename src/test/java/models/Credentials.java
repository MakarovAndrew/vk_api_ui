package models;

import lombok.Getter;
import lombok.NoArgsConstructor;

@NoArgsConstructor @Getter
public class Credentials {
    private String login;
    private String password;
    private String token;
}