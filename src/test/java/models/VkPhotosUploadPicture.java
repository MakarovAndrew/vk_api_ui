package models;

import lombok.Getter;
import lombok.NoArgsConstructor;

@NoArgsConstructor @Getter
public class VkPhotosUploadPicture {
    private String server;
    private String photo;
    private String hash;
}