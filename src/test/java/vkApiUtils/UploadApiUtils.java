package vkApiUtils;

import models.VkPhotosUploadPicture;
import okhttp3.HttpUrl;
import utils.HttpUtils;
import utils.JsonUtils;
import java.io.File;
import java.util.Map;

public class UploadApiUtils extends BaseApiUtils{

    public static String getWallUploadServerUrl() {
        final HttpUrl url = getFullUrl("photos.getWallUploadServer", null);
        return JsonUtils.getValueByKeyFromJsonString(
                HttpUtils.sendPostRequest(url), "upload_url"
        );
    }

    public static VkPhotosUploadPicture uploadPictureToServer(String uri, File file) {
        HttpUrl url = HttpUrl.parse(uri);
        return JsonUtils.readObjectFromJsonString(
                HttpUtils.sendPostRequest(url, file), VkPhotosUploadPicture.class
        );
    }

    public static String savePictureToServer(VkPhotosUploadPicture vkPhotosUploadPicture) {
        final Map<String,String> queryParameters = Map.of(
                "photo", vkPhotosUploadPicture.getPhoto(),
                "server", vkPhotosUploadPicture.getServer(),
                "hash", vkPhotosUploadPicture.getHash()
        );
        final HttpUrl url = getFullUrl("photos.saveWallPhoto", queryParameters);
        return JsonUtils.getValueByKeyFromJsonString(
                HttpUtils.sendPostRequest(url), "id"
        );
    }
}