package forms;

import aquality.selenium.elements.interfaces.IButton;
import aquality.selenium.elements.interfaces.ITextBox;
import org.openqa.selenium.By;

public class VkLoginForm extends BaseForm {
    private final ITextBox inputLoginTextBox = getElementFactory().getTextBox(By.className("VkIdForm__input"), "The 'Login' field");
    private final IButton signInButton = getElementFactory().getButton(By.xpath("//button[contains(@class, 'VkIdForm__signInButton')]"), "The 'Sign in' button");

    public VkLoginForm() {
        super(By.id("index_login"), "VK login form");
    }

    public void enterLogin(String login) {
        inputLoginTextBox.clearAndType(login);
    }

    public void clickSignInButton() {
        signInButton.click();
    }
}