package utils;

import aquality.selenium.core.logging.Logger;
import okhttp3.*;
import java.io.File;
import java.io.IOException;
import java.time.Duration;
import java.util.List;
import static utils.HttpUtilsConfigManager.settings;

public class HttpUtils {
    private final static int TIMEOUT = Integer.parseInt(settings.getValue("/timeouts/timeoutRequest").toString());
    private final static OkHttpClient client = new OkHttpClient.Builder()
            .readTimeout(Duration.ofSeconds(TIMEOUT))
            .protocols(List.of(Protocol.HTTP_1_1))
            .build();

    public static String sendPostRequest(HttpUrl url) {
        Logger.getInstance().info("Sending HTTP POST request :: '%s' ...", url);
        RequestBody emptyBody = RequestBody.create(new byte[0]);
        return sendPostRequest(url, emptyBody);
    }

    public static String sendPostRequest(HttpUrl url, File file) {
        Logger.getInstance().info("Sending file '%s' via HTTP POST request :: '%s' ...", file.getName(), url);
        RequestBody requestBody = new MultipartBody.Builder()
                .setType(MultipartBody.FORM)
                .addFormDataPart(
                        "file", file.getName(), RequestBody.create(
                                file, MediaType.parse("multipart/form-data")
                        )
                )
                .build();
        return sendPostRequest(url, requestBody);
    }

    private static String sendPostRequest(HttpUrl url, RequestBody body) {
        Request request = new Request.Builder()
                .url(url)
                .post(body)
                .build();
        try {
            return client.newCall(request).execute().body().string();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}